﻿using FakeXiecheng.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FakeXiecheng.API.Dtos;

namespace FakeXiecheng.API.Services
{
   public interface ITouristRouteRepository
    {
        //获得所有旅游路线
        Task<IEnumerable<TouristRoute>> GetTouristRoutesAsync(string keyword,string ratingOperator, int? raringValue);

        //返回单独的旅游路线
        Task<TouristRoute> GetTouristRouteAsync(Guid toursitRouteId);

        Task<bool> TouristRouteExistsAsync(Guid toursitRouteId);

        Task<IEnumerable<TouristRoutePicture>> GetPicturesByTouristRouteIdAsync(Guid toursitRouteId);

        Task<TouristRoutePicture> GetPictureAsync(int pictureId);

        void  AddTouristRoute(TouristRoute touristRoute);

        void AddTouristRoutePicture(Guid toursitRouteId,TouristRoutePicture touristRoutePicture);

        void DeleteTouristRoute(TouristRoute touristRoute);

        void DeleteTouristRoutePicture(TouristRoutePicture picture);

        Task<ShoppingCart> GetShoppingCartByUserId(string userId);

        Task CreateShoppingCart(ShoppingCart shoppingCart);

        Task AddShoppingCartItem(LineItem lineItem);

        Task<LineItem> GetShoppingCartItemByItemId(int itemId);

        void DeleteShoppingCartItem(LineItem lineItem);

        Task<IEnumerable<LineItem>> GetShoppingCartsByIdListAsync(IEnumerable<int> ids);

        void DeleteShoppingCartItems(IEnumerable<LineItem> lineItems);

        Task AddOrderAsync(Order order);

        Task<IEnumerable<Order>> GetOrdersByUserId(string userId);

        Task<Order> GetOrderById(Guid orderId);

        Task<bool> SaveAsync();
    }
}
