﻿using FakeXiecheng.API.Database;
using FakeXiecheng.API.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FakeXiecheng.API.Services
{
    public class TouristRouteRepository : ITouristRouteRepository
    {
        private readonly AppDbContext _context;

        public TouristRouteRepository(AppDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// 添加一条旅游路线
        /// </summary>
        /// <param name="touristRoute"></param>
        public void AddTouristRoute(TouristRoute touristRoute)
        {
            if (touristRoute == null)
            {
                throw new ArgumentNullException(nameof(touristRoute));
            }
            _context.TouristRoutes.Add(touristRoute);
        }

        /// <summary>
        /// 添加旅游路线图片资源
        /// </summary>
        /// <param name="toursitRouteId"></param>
        /// <param name="touristRoutePicture"></param>
        public void AddTouristRoutePicture(Guid toursitRouteId, TouristRoutePicture touristRoutePicture)
        {
            if (toursitRouteId == Guid.Empty) 
            {
                throw new ArgumentNullException(nameof(touristRoutePicture));
            }
            touristRoutePicture.TouristRouteId = toursitRouteId;
            _context.TouristRoutePictures.Add(touristRoutePicture);
        }

        public void DeleteTouristRoute(TouristRoute touristRoute)
        {
            _context.TouristRoutes.Remove(touristRoute);
        }

        public void DeleteTouristRoutePicture(TouristRoutePicture picture)
        {
            _context.TouristRoutePictures.Remove(picture);
        }

        /// <summary>
        /// 获取一条旅游路线图片资源
        /// </summary>
        /// <param name="pictureId"></param>
        /// <returns></returns>
        public async Task<TouristRoutePicture> GetPictureAsync(int pictureId)
        {
            return await _context.TouristRoutePictures.Where(p => p.Id == pictureId).FirstOrDefaultAsync(); ;
        }

        /// <summary>
        /// 获取旅游路线图片资源
        /// </summary>
        /// <param name="toursitRouteId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<TouristRoutePicture>> GetPicturesByTouristRouteIdAsync(Guid toursitRouteId)
        {
            return await _context.TouristRoutePictures.Where(p => p.TouristRouteId == toursitRouteId).ToListAsync();
        }

        /// <summary>
        /// 获取一条旅游路线
        /// </summary>
        /// <param name="toursitRouteId"></param>
        /// <returns></returns>
        public async Task<TouristRoute> GetTouristRouteAsync(Guid toursitRouteId)
        {
            return await _context.TouristRoutes
                .Include(t => t.TouristRoutePictures)
                .FirstOrDefaultAsync(t => t.Id == toursitRouteId);
        }
        /// <summary>
        /// 自定义搜索条件
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="ratingOperator"></param>
        /// <param name="raringValue"></param>
        /// <returns></returns>
        public async Task<IEnumerable<TouristRoute>> GetTouristRoutesAsync(
            string keyword,
            string ratingOperator,
            int? raringValue
            )
        {
            IQueryable<TouristRoute> result = _context
                .TouristRoutes
                .Include(t => t.TouristRoutePictures);
            if (!string.IsNullOrWhiteSpace(keyword))
            {
                keyword = keyword.Trim();
                result = result.Where(t => t.Title.Contains(keyword));
            }
            if (raringValue >= 0)
            {
                switch (ratingOperator)
                {
                    case "largerThan":
                        result = result.Where(t => t.Rating >= raringValue);
                        break;
                    case "lessThan":
                        result = result.Where(t => t.Rating <= raringValue);
                        break;
                    case "equalTo":
                        result = result.Where(t => t.Rating == raringValue);
                        break;
                }
            }
            return await result.ToListAsync();
        }

        public async Task<ShoppingCart> GetShoppingCartByUserId(string userId)
        {
            return await _context.ShpShoppingCarts
                .Include(s => s.User)
                .Include(s => s.ShoppingCartItems).ThenInclude(li => li.TouristRoute)
                .Where(s => s.UserId == userId)
                .FirstOrDefaultAsync();
        }

        public async Task CreateShoppingCart(ShoppingCart shoppingCart)
        {
           await _context.ShpShoppingCarts.AddAsync(shoppingCart);
        }

        public async Task AddShoppingCartItem(LineItem lineItem) 
        {
            await _context.LineItems.AddAsync(lineItem);
        }

        public async Task<LineItem> GetShoppingCartItemByItemId(int itemId)
        {
            return await _context.LineItems
                .Where(li => li.Id == itemId)
                .FirstOrDefaultAsync();
        }

        public void DeleteShoppingCartItem(LineItem lineItem)
        {
            _context.LineItems.Remove(lineItem);
        }

        public async Task<IEnumerable<LineItem>> GetShoppingCartsByIdListAsync(IEnumerable<int> ids) 
        {
            return await _context.LineItems
                .Where(li => ids.Contains(li.Id))
                .ToListAsync();
        }

        public void DeleteShoppingCartItems(IEnumerable<LineItem> lineItems) 
        {
            _context.LineItems.RemoveRange(lineItems);
        }

        public async Task AddOrderAsync(Order order)
        {
           await _context.Orders.AddAsync(order);
        }

        public async Task<IEnumerable<Order>> GetOrdersByUserId(string userId)
        {
           return await _context.Orders.Where(o => o.UserId == userId).ToListAsync();
        }

        public async Task<Order> GetOrderById(Guid orderId)
        {
            return await _context.Orders
                .Include(o => o.OrderItems)
                .ThenInclude(oi => oi.TouristRoute)
                .Where(o => o.Id == orderId).FirstOrDefaultAsync();
        }

        /// <summary>
        /// 保存到数据库
        /// </summary>
        /// <returns>返回成功条数是否大于0</returns>
        public async Task<bool> SaveAsync()
        {
            return (await _context.SaveChangesAsync() >= 0);
        }

        /// <summary>
        /// 验证旅游路线是否存在
        /// </summary>
        /// <param name="toursitRouteId">旅游路线Id</param>
        /// <returns></returns>
        public async Task<bool> TouristRouteExistsAsync(Guid toursitRouteId)
        {
            return await _context.TouristRoutes.AnyAsync(t => t.Id == toursitRouteId);
        }
    }
}
