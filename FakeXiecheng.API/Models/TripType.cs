﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FakeXiecheng.API.Models
{
    public enum TripType
    {
        //酒店+景点
        HotelAndAttractions,
        //跟团游
        Group,
        //私家团
        PrivateGroup,
        //自由行
        BackPackTour,
        //半自助游
        SemiBackPackTour
    }
}
