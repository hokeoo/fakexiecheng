﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FakeXiecheng.API.Models
{
    public enum TravelDays
    {
        //1天
        One,
        //2天
        Two,
        //3天
        Three,
        //4天
        Four,
        //5天
        Five,
        //6天
        Six,
        //7天
        Seven,
        //8天
        Eight,
        //8天以上
        EightPlus
    }
}
