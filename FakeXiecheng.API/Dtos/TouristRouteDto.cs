﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FakeXiecheng.API.Dtos
{
    public class TouristRouteDto
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        //价格计算:原价 * 折扣
        public decimal Price { get; set; }
        //public decimal OriginalPrice { get; set; }
        //public double? DiscountPresent { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }
        public DateTime? DepartureTime { get; set; }
        public string Features { get; set; }
        public string Fees { get; set; }
        public string Notes { get; set; }
        //评分
        public double? Rating { get; set; }
        //出游天数
        public string TravelDays { get; set; }
        //旅游类型
        public string TripType { get; set; }
        //出发城市
        public string DepartureCity { get; set; }

        public ICollection<TouristRoutePictureDto> TouristRoutePictures { get; set; }
          = new List<TouristRoutePictureDto>();
    }
}
